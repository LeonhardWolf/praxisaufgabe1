import numpy as np
import sounddevice as sd
import time

globalSamplerate = 44.1e3
playbackMs = 2000
testFrequencies = [50, 100, 200, 500, 1e3, 2e3, 5e3, 10e3, 20e3]

def absoluteToDecibel(referenceValue, inputValue):
    """Rechnet absolute Feldgrößen in Dezibel um.

    Args:
        referenceValue (float/int): Der Bezugswert.
        inputValue (float/int): Der Wert, der in einen Pegel umgerechnet werden soll.

    Returns:
        float/int: Den Pegel.
    """
    decibelValue = 20*np.log10(inputValue/referenceValue)
    return decibelValue

def dBToAbsolute(referenceValue, inputdB):
    """Rechnet Dezibel in absolute Felgrößen um.

    Args:
        referenceValue (int/float): Der Bezugswert.
        inputdB ([float/int): Der Pegel, der in eine absolute Feldgröße umgerechnet wird.

    Returns:
        float/nint: Die berechnete Feldgröße.
    """
    absoluteValue = referenceValue * 10**(inputdB/20)
    return absoluteValue

def generateTestLevels(mindB, maxdB, stepdB):
    """Generiert ein np.array mit Testpegeln entsprechend der gesetzen Parameter.

    Args:
        mindB (float/int): Minimalpegel aller Werte.
        maxdB (float/int): Maximalpegel aller Werte.
        stepdB (float/int): Die Schrittgröße für die Liste der Pegel.

    Returns:
        np.array: Das np.arrray, das die Testpegel enthält.
    """
    absoluteValues = []
    dBLevels = np.arange(start=mindB, stop=maxdB+1, step=stepdB)
    for i in dBLevels:
        absoluteValues.append(dBToAbsolute(0.5, i))
    return absoluteValues

def generateSineArray(frequency, level, samplerate, length=playbackMs):
    """Generiert das Array, das die Abtastwerte für das Sinussignal enthält.

    Args:
        frequency (int): Die Frequenz des Sinussignals.
        level (int/float): Der Pegel des Sinussignals.
        samplerate (int): Die Abtastrate der erzeugten Schwingung.

    Returns:
        np.array: Das Array, dass die Abtastwerte für die definierte Sinusschwinung enthält.
    """
    testXAxis = np.linspace(start=0, stop=length, num=int(length*samplerate/1000))
    sineArray = level*np.sin(2*np.pi*frequency*testXAxis/1000)
    return sineArray

def playTestLevels(frequency, levels):
    """Spielt die Sinussignale mit einer gegebenen Frequenz mit verschiedenen Pegeln ab.

    Args:
        frequency (int): Die Frequenz der abzuspielenden Sinusschwingung.
        levels (np.array): Das Array, das die Pegelwerte enthält, in denen die Sinusschwingung abgespielt werden soll.
    """
    for level in levels:
        soundArray = generateSineArray(frequency=frequency, level=level, samplerate=globalSamplerate)
        print("level: {} dB".format(absoluteToDecibel(referenceValue=0.5, inputValue=level)))
        sd.play(data=soundArray, samplerate=globalSamplerate)
        sd.wait()
        time.sleep(playbackMs//(2*1000))

def playTestTones():
    """Spielt die Testsignale ab. Pro Testfrequenz werden alle Testpegel durchlaufen.
    """
    levels = generateTestLevels(mindB=-90, maxdB=-40, stepdB=5)
    for freq in testFrequencies:
        print("frequency: {}Hz".format(freq))
        playTestLevels(frequency=freq, levels=levels)

if __name__ == "__main__":
    playTestTones()