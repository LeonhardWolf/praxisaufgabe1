import numpy as np
import matplotlib.pyplot as plt

testFrequencies = [50, 100, 200, 500, 1e3, 2e3, 5e3, 10e3, 15e3]
frequencyLevelsdB = [-50, -65, -70, -70, -80, -70, -75, -70, -55]


def setPlotSettings(axis):
    """Hier wird die Formatierung des Plots vorgenommen.

    Args:
        axis (matplotlib.axes): Die Matplotlib-Achse, die formatiert werden soll.
    """
    axis.set_xlim(left=20, right=20e3, auto=False)
    axis.set_xlabel("Frequenz [Hz]")
    axis.set_ylabel("Pegel [dB]")
    plt.xscale(value="log")
    plt.grid(b=True,  which="both", axis="both")
    plt.title(label="Experimentell bestimmte Hörschwelle")

def normalizeLevelData(levelArray, factor):
    """Addiert zu allen Elementen eines Arrays den gegebenen Parameter.

    Args:
        levelArray (np.array): Das Array, dessen Elementwerte angepasst werden sollen.
        factor (int/float): Wert, der zu allen Elementwerten des Arrays addiert wird.

    Returns:
        np.array: Das Array, zu dessen Elementwerte ausgehend vom levelArray der Wert factor addiert wurde.
    """
    outputArray = []
    for i in levelArray:
        outputArray.append(i+factor)
    return outputArray

def plotHearingThreshold():
    """Die zuvor definierten Variablen mit den Messergebnissen werden geplottet.
    """
    testLevelsdB = normalizeLevelData(levelArray=frequencyLevelsdB, factor=-frequencyLevelsdB[4])
    fig = plt.figure(num="Plot Hörschwelle")
    ax1 = fig.add_subplot(111)
    plt.plot(testFrequencies, testLevelsdB, "o", linestyle=":")
    setPlotSettings(axis=ax1)
    plt.show()

if __name__ == "__main__":
    plotHearingThreshold()